# -*- coding: utf-8 -*-
from __future__ import print_function, division, absolute_import

import os
import sys
import argparse
import numpy as np
import cv2


def parse_arguments():
    parser = argparse.ArgumentParser(description='',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--dir', help='input directory', required=True)
    parser.add_argument('--types', help='tl tr bl br image directory', nargs=4, default=['img', 'Octane',
                                                                                         'flowBackward', 'instance'])
    parser.add_argument('--out_dir', help='', required=True)

    return parser.parse_args()

def get_files(path, ext=None):
    """ Get list of files in directory.  Possibly filter by extension.

    Args:
        path - directory to list files from
        ext  - set / list of extensions like ".png"
    """
    files = sorted([f for f in next(os.walk(path))[2]])
    if ext is not None:
        files = [f for f in files if os.path.splitext(f)[1].lower() in ext]

    return files

def get_image_number(img_name):
    return int(os.path.splitext(img_name)[0].split('_')[-1])

def get_images(path):
    images = get_files(path, ext=['.png', '.jpg', '.jpeg'])
    numbered = {get_image_number(img_name): os.path.join(path, img_name)
                for img_name in images}
    return numbered

def compose(tl, tr, bl, br, center_x, center_y):
    canvas = np.zeros((tl.shape[0], tl.shape[1], 3), dtype=np.uint8)

    canvas[:center_y, :center_x, :] = tl[:center_y, :center_x, :]
    canvas[:center_y, center_x:, :] = tr[:center_y, center_x:, :]
    canvas[center_y:, :center_x, :] = bl[center_y:, :center_x, :]
    canvas[center_y:, center_x:, :] = br[center_y:, center_x:, :]
    return canvas

def run(args):
    tl_path = os.path.join(args.dir, args.types[0])
    tr_path = os.path.join(args.dir, args.types[1])
    bl_path = os.path.join(args.dir, args.types[2])
    br_path = os.path.join(args.dir, args.types[3])

    tl_images = get_images(tl_path)
    tr_images = get_images(tr_path)
    bl_images = get_images(bl_path)
    br_images = get_images(br_path)

    tl_frames = set(tl_images.keys())
    tr_frames = set(tr_images.keys())
    bl_frames = set(bl_images.keys())
    br_frames = set(br_images.keys())

    available_frames = sorted(tl_frames & tr_frames & bl_frames & br_frames)

    center_x = 250
    center_y = 250

    if not os.path.exists(args.out_dir):
        os.makedirs(args.out_dir)

    for frame in available_frames:
        tl = cv2.imread(tl_images[frame])
        tr = cv2.imread(tr_images[frame])
        bl = cv2.imread(bl_images[frame])
        br = cv2.imread(br_images[frame])

        center_x = tl.shape[1] // 2
        center_y = tl.shape[0] // 2

        composition = compose(tl, tr, bl, br, center_x, center_y)
        cv2.imshow("cv: composition", composition)

        out_path = os.path.join(args.out_dir, '{:08d}.png'.format(frame))
        cv2.imwrite(out_path, composition)
        c = cv2.waitKey(5)
        if c == ord('q'):
            break

    return 0

def main():
    args = parse_arguments()
    return run(args)

if __name__ == '__main__':
    sys.exit(main())
